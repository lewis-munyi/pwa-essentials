var CACHE_NAME = 'cache'; var resources = [
  '/images/logo.jpg',
  '/manifest.json',
  'index.html' ]; // Open + Cache + Verify cache self.addEventListener('install', 
function(event) {
  // Perform install steps
  event.waitUntil(
    caches.open(CACHE_NAME)
      .then(function(cache) {
        // console.log('Opened cache');
        return cache.addAll(resources);
      })
  );
});
// Load cache if present otherwise install self.addEventListener('fetch', 
function(event) {
    event.respondWith(
      caches.match(event.request)
        .then(function(response) {
          // Cache hit - return response
          if (response) {
            return response;
          }
          return fetch(event.request);
        }
      )
    );
  });
